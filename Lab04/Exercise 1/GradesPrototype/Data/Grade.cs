﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GradesPrototype.Data
{
    // Types of user
    public enum Role { Teacher, Student };

    // WPF Databinding requires properties

    // TODO: Exercise 1: Task 1a: Convert Grade into a class and define constructors
    public class Grade
    {
        public int StudentID { get; set; }
        public string AssessmentDate { get; set; }
        public string SubjectName { get; set; }
        public string Assessment { get; set; }
        public string Comments { get; set; }
    }

    // TODO: Exercise 1: Task 2a: Convert Student into a class, make the password property write-only, add the VerifyPassword method, and define constructors
    public class Student
    {
        public Student()
        {

        }

        public int StudentID { get; set; }
        public string UserName { get; set; }
        public string Password { private get; set; }
        public int TeacherID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public bool VerifyPassword(string Password, string compare)
        {
            if (String.Compare(Password, compare) == 0)
                return true;
            else
                return false;
        }

        public string GetPassword
        {
            get { return this.Password; }
        }
    }

    // TODO: Exercise 1: Task 2b: Convert Teacher into a class, make the password property write-only, add the VerifyPassword method, and define constructors
    public class Teacher
    {
        public Teacher()
        {

        }

        public int TeacherID { get; set; }
        public string UserName { get; set; }
        public string Password { private get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Class { get; set; }

        public bool VerifyPassword(string Password, string compare)
        {
            if (String.Compare(Password, compare) == 0)
                return true;
            else
                return false;
        }

        public string GetPassword
        {
            get { return this.Password; }
        }
    }
}
