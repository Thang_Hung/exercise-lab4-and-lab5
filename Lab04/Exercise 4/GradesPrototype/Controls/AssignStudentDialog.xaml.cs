﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GradesPrototype.Data;
using GradesPrototype.Services;
using System.Linq;
using GradesPrototype.Views;

namespace GradesPrototype.Controls
{
    /// <summary>
    /// Interaction logic for AssignStudentDialog.xaml
    /// </summary>
    public partial class AssignStudentDialog : Window
    {
        public event EventHandler Back;

        public AssignStudentDialog()
        {
            InitializeComponent();
        }

        // TODO: Exercise 4: Task 3b: Refresh the display of unassigned students
        private void Refresh()
        {
            list.ItemsSource = DataSource.Students.Where(x => x.TeacherID == 0).ToList();
        }

        private void AssignStudentDialog_Loaded(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        // TODO: Exercise 4: Task 3a: Enroll a student in the teacher's class
        private void Student_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn != null)
            {
                // Find out which student was clicked
                int studentID = (int)btn.Tag;

                // Find the details of the student by examining the DataContext of the Button
                Student student = (from Student std in DataSource.Students select std)
                                .Where(x => x.StudentID == studentID).SingleOrDefault();

                SessionContext.CurrentTeacher.EnrollInClass(student);

                //Refresh the display of unassigned students
                Refresh();

                //Refresh the display studentPage
                StudentsPage form = new StudentsPage();
                form.Refresh();
                form.Visibility = Visibility.Visible ;

                MessageBox.Show("Enroll Success");
                
            }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            // Close the dialog box
            this.Close();
        }
    }
}
