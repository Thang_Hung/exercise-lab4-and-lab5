using Microsoft.VisualStudio.TestTools.UnitTesting;
using GradesPrototype.Data;
using System;

namespace GradesTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestInitialize]
        public void Init()
        {
            DataSource.CreateData();
        }

        [TestMethod]
        public void TestValidGrade()
        {   
           Grade grade = new Grade(1, "02/02/2020", "Math", "B+", "ccccc");

            Assert.IsNotNull(grade);
        }

        [TestMethod]
        public void TestBadDate()
        {
            Grade grade = new Grade();

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => grade.AssessmentDate = "03/12/2022");
        }

        [TestMethod]
        public void TestDateNotRecognized()
        {
            Grade grade = new Grade();

            Assert.ThrowsException<ArgumentException>(() => grade.AssessmentDate = "03/22/2022");
        }

        [TestMethod]
        public void TestBadAssessment()
        {
            //
            Grade grade = new Grade();

            //
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => grade.Assessment = "+A");
        }

        [TestMethod]
        public void TestBadSubject()
        {
            Grade grade = new Grade();

            Assert.ThrowsException<ArgumentException>(() => grade.SubjectName = "M");
        }
    }
}
