﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GradesPrototype.Data;
using GradesPrototype.Services;

namespace GradesPrototype.Controls
{
    /// <summary>
    /// Interaction logic for ChangePasswordDialog.xaml
    /// </summary>
    public partial class ChangePasswordDialog : Window
    {
        public ChangePasswordDialog()
        {
            InitializeComponent();
        }

        // If the user clicks OK to change the password, validate the information that the user has provided
        private void ok_Click(object sender, RoutedEventArgs e)
        {
            //check value of confirm and new password
            if(confirm.Password != newPassword.Password)
            {
                MessageBox.Show("The new confirmation and password are not of the same value", "Change Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // TODO: Exercise 2: Task 4a: Get the details of the current user
            // TODO: Exercise 2: Task 4b: Check that the old password is correct for the current user
            // TODO: Exercise 2: Task 4c: Check that the new password and confirm password fields are the same
            // TODO: Exercise 2: Task 4d: Attempt to change the password
            // If the password is not sufficiently complex, display an error message

            if (SessionContext.UserRole == Role.Student)
            {
                Student std = SessionContext.CurrentStudent;
                
                if(!std.SettingPassword(newPassword.Password))
                {
                    MessageBox.Show("Invalid password", "Change Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                std.Password = newPassword.Password;

                //Change data
                DataSource.Students.Remove(SessionContext.CurrentStudent);
                DataSource.Students.Add(std);
            }     
            else
            {
                Teacher teacher = SessionContext.CurrentTeacher;

                if (!teacher.SettingPassword(newPassword.Password))
                {
                    MessageBox.Show("Invalid password", "Change Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                teacher.Password = newPassword.Password;

                //Change data
                DataSource.Teachers.Remove(SessionContext.CurrentTeacher);
                DataSource.Teachers.Add(teacher);
            }
                

            // Indicate that the data is valid
            this.DialogResult = true;
        }
    }
}
